


## extract file extension
ExtractExt <- function(inputfile) {
  tolower(sub("(.+)\\.(rda|rds|csv|txt|dpt|json|JSON)$","\\2", 
              basename(inputfile)))
}


SetPlot <- function(xlimits, ylimits){
  #  par(mar = c(3, 3, 3, 3), mgp = c(0,.5,0))
  plot.new()
  plot.window(xlim = xlimits, ylim = ylimits)
  axis(1, tck = 0.025) 
  axis(2, tck = 0.025) 
  axis(3, tck = 0.025, labels = FALSE)
  axis(4, tck = 0.025, labels = FALSE) 
  box()
}

Compute.Spectra.stats <- function(subtable, wavenumber) { 
  m <- colMeans(subtable)
  s <- apply(subtable, 2, sd)
  n <- nrow(subtable)
  t <- qt(.975, n)
  data.frame(
    wavenumber = wavenumber,
    mean = m,
    conf.lower = m - t * s / sqrt(n),
    conf.upper = m + t * s / sqrt(n),
    sd.lower = m - s,
    sd.upper = m + s)
}

Plot.Spectra.basic <- function(spec, title.main = "Plot", 
                               segment1 = NULL, segment2 = NULL){
  
  
  spec <- na.omit(spec)
  spec.mean <- colMeans(spec)
  spec.n <- nrow(spec)
  spec.sd <- apply(spec, 2, sd)
  # calc 95% confidence interval
  spec.t <- qt(.975, spec.n)
  conf.lower <- spec.mean - spec.t * spec.sd / sqrt(spec.n)
  conf.upper <- spec.mean + spec.t * spec.sd / sqrt(spec.n)
  
  # plot regressors mean
  xlimits <- c(max(Wavenumbers(spec)), min(Wavenumbers(spec)))
  ylimits <- c(min(spec.mean), max(spec.mean))
  SetPlot(xlimits, ylimits)
  points(Wavenumbers(spec), spec.mean, cex = 0.15)
  # 95% confidence intervals
  polygon( c(rev(Wavenumbers(spec)), Wavenumbers(spec)), c(rev(conf.upper), conf.lower), 
           col = rgb(0, 0, 0, alpha = 0.2), border = NA)
  abline(h = 0, col = "gray")
  if (!is.null(segment1)) {
    
    segment1.x <- Wavenumbers(spec)[Wavenumbers(spec) >= min(segment1) &
                                      Wavenumbers(spec) <= max(segment1)]
    segment1.y <- rep(1, length(segment1.x))
    polygon( c(rev(segment1.x), segment1.x),
             c(rev(segment1.y * min(ylimits)), segment1.y * max(ylimits)),
             col = rgb(1, 0, 0, alpha = 0.2), border = NA)
  }
  if (!is.null(segment2)) {
    
    segment2.x <- Wavenumbers(spec)[Wavenumbers(spec) >= min(segment2) &
                                      Wavenumbers(spec) <= max(segment2)]
    segment2.y <- rep(1, length(segment2.x))
    polygon( c(rev(segment2.x), segment2.x),
             c(rev(segment2.y * min(ylimits)), segment2.y * max(ylimits)),
             col = rgb(0, 0, 1, alpha = 0.2), border = NA)
  }
  title(main = title.main, 
        xlab = expression(paste("Wavenumber (cm" ^ "-1",")")),
        ylab = "Mean absorbance")
}

Plot.Spectra.lines <- function(spec, title.main = "Plot", xlim = NULL, ylim = NULL) { 
  if (is.null(xlim)) {
    xlim <- c(max(Wavenumbers(spec)), min(Wavenumbers(spec)))
  }
  if (is.null(ylim)) {
    ylim <- c(min(spec), max(spec))
  }
  SetPlot(xlim, ylim)
  abline(h = 0, col = "gray")
  
  matlines(Wavenumbers(spec), t(spec), lwd = 2)

  title(main = title.main, 
        xlab = expression(paste("Wavenumber (cm" ^ "-1",")")),
        ylab = "Absorbance")
  
  
}

Plot.parameter.selection.ssb <- function(data.table, segment.name) {
  SetPlot(xlimits = c(0, max(data.table$param)), 
          ylimits = c(0, max(data.table$Median)))
  lines(data.table$param, data.table$Median, lwd = 3)
  points(data.table$param, data.table$Median, pch = "o", cex = 1.5)
  title(main = segment.name, xlab = "edf parameter", ylab = "NAF (%)")
}

Plot.error.message <- function(filename) {
  plot.new()
  plot.window(xlim = c(0, 1), ylim = c(0, 1))
  text(0.5, 0.95, 
       labels = paste("Something went wrong, please check the log file (",
                      filename, 
                      ") by clicking the Download results button.", sep = ""), 
       cex = 1, col = "red")
  
}



Prepare.ssb.call <- function(ssb.dir) {
  write(sprintf("Rscript %s %s 2>> %s\n
                Rscript %s %s 2>> %s\n
                Rscript %s %s 2>> %s\n
                Rscript %s %s 2>> %s\n",
                # main_fits_db
                file.path(path.package("APRLssb", quiet = FALSE),
                          "example_files", "main_fits_db.R"),
                file.path(ssb.dir, "input_ssb_internal.json"),
                file.path(ssb.dir, "ssb_log.txt"),
                    
                # plot_fits.R call
                file.path(path.package("APRLssb", quiet = FALSE),
                          "example_files", "plot_fits.R"),
                file.path(ssb.dir, "input_ssb_internal.json"),
                file.path(ssb.dir, "ssb_log.txt"),
                
                    # eval_fits_db.R call
                file.path(path.package("APRLssb", quiet = FALSE),
                          "example_files", "eval_fits_db.R"),
                file.path(ssb.dir, "input_ssb_internal.json"),
                file.path(ssb.dir, "ssb_log.txt"),
                
                # eval_plots.R call
                file.path(path.package("APRLssb", quiet = FALSE),
                          "example_files", "eval_plots.R"),
                file.path(ssb.dir, "input_ssb_internal.json"),
                file.path(ssb.dir, "ssb_log.txt")
                ),
            file = file.path(ssb.dir, "call_ssb.sh"))
}

Prepare.mpf.call <- function(mpf.dir) {
  write(sprintf("Rscript %s %s 2>> %s\n
                Rscript %s %s 2>> %s\n
                Rscript %s %s 2>> %s",
                # main_fits_constr_cCOH.R call
                file.path(path.package("APRLmpf", quiet = FALSE),
                          "example_files", "main_fits_constr_cCOH.R"),
                file.path(mpf.dir, "input_mpf_internal.json"),
                file.path(mpf.dir, "mpf_log.txt"),
                
                # plot_fits.R call
                file.path(path.package("APRLmpf", quiet = FALSE),
                          "example_files", "plot_fits.R"),
                file.path(mpf.dir, "input_mpf_internal.json"),
                file.path(mpf.dir, "mpf_log.txt"),
                
                # eval_fits_db.R call
                file.path(path.package("APRLmpf", quiet = FALSE),
                          "example_files", "conv_areas.R"),
                file.path(mpf.dir, "input_mpf_internal.json"),
                file.path(mpf.dir, "mpf_log.txt")
  ),
  file = file.path(mpf.dir, "call_mpf.sh"))
}


Prepare.mvr.call <- function(mvr.dir) {
  write(sprintf("Rscript %s %s 2>> %s\n
                Rscript %s %s 2>> %s\n",
                # main_fits.R call
                file.path(path.package("APRLmvr", quiet = FALSE),
                          "example_files", "main_fits.R"),
                file.path(mvr.dir, "input_mvr_internal.json"),
                file.path(mvr.dir, "mvr_log.txt"),
                
                # eval_fits.R call
                file.path(path.package("APRLmvr", quiet = FALSE),
                          "example_files", "eval_fits.R"),
                file.path(mvr.dir, "input_mvr_internal.json"),
                file.path(mvr.dir, "mvr_log.txt")
  ),
  file = file.path(mvr.dir, "call_mvr.sh"))
}

